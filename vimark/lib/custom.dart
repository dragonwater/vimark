import 'package:flutter/material.dart';



String GetThumbnailUrl(String id) {
  return "https://img.youtube.com/vi/$id/mqdefault.jpg";
}

String GetIdFromUrl(String url) {
  if(url.length==11)
    return url;
  else if (url.contains("youtu.be")) {
    int start = url.indexOf(".be/") + 4;
    return url.substring(start, start + 11);
  }
  else {
    if(url.contains("?v=")) {
      int start = url.indexOf("?v=") + 3;
      return url.substring(start, start + 11);
    }
    else
      return "Error";
  }
}

String DurationToString(Duration duration) {
  String twoDigits(int n) {
    if(n < 10)
      return "0$n";
    else
      return "$n";
  }

  String twoDigitMinutes = twoDigits(duration.inMinutes.remainder(60));
  String twoDigitSeconds = twoDigits(duration.inSeconds.remainder(60));
  return "${twoDigits(duration.inHours)}:$twoDigitMinutes:$twoDigitSeconds";
}

Duration StringToDuration(String string) { //hh:mm:ss
  List<String> times = string.split(":");
  int hh = int.parse(times[0]);
  int mm = int.parse(times[1]);
  int ss = int.parse(times[2]);
  return Duration(hours: hh, minutes: mm, seconds: ss);
}


class SkipStatusBar extends StatelessWidget {
  final Widget child;
  SkipStatusBar({this.child});
  @override
  Widget build(BuildContext context) {
    final double statusBarHeight = MediaQuery.of(context).padding.top;
    return new Padding(
        padding: new EdgeInsets.only(top: statusBarHeight),
        child: child
    );
  }
}

class DrawerButton extends StatelessWidget {
  final Icon icon;
  final String text;
  final Function onPressed;
  DrawerButton({this.icon, this.text, this.onPressed});

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: onPressed,
      child: Container(
        height: 50,
        child: Padding(
          padding: const EdgeInsets.fromLTRB(20, 0, 0, 0),
          child: Row(
            children: <Widget>[
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 25.0),
                child: icon,
              ),
              Text(text),
            ],
          ),
        ),
      ),
    );
  }
}

