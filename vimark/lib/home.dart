import 'package:flutter/material.dart';
import 'package:vimark/add_comment.dart';
import 'custom.dart';
import 'storage.dart';


class HomePage extends StatefulWidget {
  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  List<VideoMemoPreview> previewList;
  void initState() {
    super.initState();
    previewList = List<VideoMemoPreview>();
    previewList.add(VideoMemoPreview(videoID: GetIdFromUrl("lkF0TQJO0bA")));
    previewList.add(VideoMemoPreview(videoID: GetIdFromUrl("v1-gGo6QRLs")));
    previewList.add(VideoMemoPreview(videoID: GetIdFromUrl("MxxlVrI1svc")));
    previewList.add(VideoMemoPreview(videoID: GetIdFromUrl("zCuv0QZICfA")));
    previewList.add(VideoMemoPreview(videoID: GetIdFromUrl("VjhuT2yco3M")));
    previewList.add(VideoMemoPreview(videoID: GetIdFromUrl("s9_2ukt-FN0")));
  }
  @override
  Widget build(BuildContext buildContext) {
    return SkipStatusBar(
      child: Scaffold(
        primary: false,
        drawer: SizedBox(
          width: 250,
          child: Drawer(
            child: Column(
              children: <Widget>[
                Padding(
                  padding: EdgeInsets.fromLTRB(0, 50, 0, 50),
                  child: Center(
                    child: Text(
                      "pindeo",
                      style: TextStyle(
                        fontFamily: "Baloo",
                        fontSize: 48,
                      ),
                    ),
                  ),
                ),
                DrawerButton(
                  icon: Icon(Icons.menu),
                  text: "모든 메모",
                  onPressed: () {

                  },
                ),
                DrawerButton(
                  icon: Icon(Icons.star),
                  text: "별표된 메모",
                  onPressed: () {

                  },
                ),
                DrawerButton(
                  icon: Icon(Icons.people),
                  text: "공동 작업 중",
                  onPressed: () {

                  },
                ),
                DrawerButton(
                  icon: Icon(Icons.link),
                  text: "링크 공유됨",
                  onPressed: () {

                  },
                ),
                Expanded(child: Container()),
                DrawerButton(
                  icon: Icon(Icons.delete),
                  text: "삭제된 메모",
                  onPressed: () {

                  },
                ),
                DrawerButton(
                  icon: Icon(Icons.settings),
                  text: "설정",
                  onPressed: () {

                  },
                ),
              ],
            ),
          ),
        ),
        bottomNavigationBar: BottomAppBar(
          child: Builder(
            builder: (scaffoldContext) => Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                IconButton(
                  icon: Icon(Icons.menu),
                  onPressed: () {
                    Scaffold.of(scaffoldContext).openDrawer();
                  },
                ),
                Row(
                  children: <Widget>[
                    Text("모든 메모"),
                    Icon(Icons.arrow_drop_up),
                  ],
                ),
                IconButton(
                  icon: Icon(Icons.add),
                  onPressed: () {
                    Navigator.of(context).push(MaterialPageRoute(builder: (context) => AddCommentPage()));
                  },
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}


class VideoMemoPreview extends StatelessWidget {
  String videoID;
  VideoMemoPreview({this.videoID});
  @override
  Widget build(BuildContext context) {
    return Container(
      child: Column(
        children: <Widget>[
          Text(videoID),
          Image.network(GetThumbnailUrl(videoID)),
        ],
      ),
    );
  }
}

class AllMemoes extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container();
  }
}

class StaredMemoes extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container();
  }
}

class SharedMemoes extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container();
  }
}

class ShreadLinks extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container();
  }
}