import 'package:flutter/material.dart';
import 'package:vimark/comment.dart';
import 'custom.dart';

class AddCommentPage extends StatefulWidget {
  @override
  _AddCommentPageState createState() => _AddCommentPageState();
}

class _AddCommentPageState extends State<AddCommentPage> {
  TextEditingController _urlController;

  void initState() {
    super.initState();
    _urlController = TextEditingController();
  }

  @override
  Widget build(BuildContext context) {
    return SkipStatusBar(
      child: Scaffold(
        body: Center(
          child: Padding(
            padding: const EdgeInsets.symmetric(horizontal: 20),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Text(
                  "Youtube",
                  style: TextStyle(
                    fontFamily: "NotoSansKR",
                    color: Colors.red,
                    fontSize: 42,
                    fontWeight: FontWeight.w100
                  )
                ),
                Text(
                  "동영상\n링크를 입력",
                  style: TextStyle(
                    fontFamily: "NotoSansKR",
                    color: Colors.grey[800],
                    fontSize: 42,
                    fontWeight: FontWeight.w100
                  ),
                ),
                Container(height: 60),
                Row(
                  children: <Widget>[
                    Flexible(
                      child: TextField(
                        controller: _urlController,
                        onSubmitted: _onSubmitted,
                      ),
                    ),
                    Container(
                      margin: EdgeInsets.symmetric(horizontal: 4),
                      child: IconButton(
                        color: Colors.lightBlueAccent,
                        icon: Icon(Icons.send),
                        onPressed: () => _onSubmitted(_urlController.text),
                      ),
                    )
                  ],
                )
              ],
            ),
          ),
        ),
      ),
    );
  }
  void _onSubmitted(String text) {
    String id = GetIdFromUrl(text);
    Navigator.of(context).pushReplacement(MaterialPageRoute(builder: (context) => CommentPage(videoId: id)));
    _urlController.clear();
  }
}
