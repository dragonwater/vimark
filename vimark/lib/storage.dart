import 'dart:async';
import 'package:sqflite/sqflite.dart';
import 'package:path/path.dart';
import 'custom.dart';

class CommentData {
  final int id;
  final String videoId;
  final String title;
  final String content;
  final Duration timeStamp;
  CommentData({this.id, this.videoId, this.title, this.content, this.timeStamp});

  @override
  String toString() {
    return "[${DurationToString(timeStamp)}] <$title> {$content}";
  }

  Map<String, dynamic> toMap() {
    return {
      'id': id,
      'video_id': videoId,
      'title': title,
      'content': content,
      'timeStamp': DurationToString(timeStamp),
    };
  }

  bool operator > (CommentData m) {
    return timeStamp.inSeconds > m.timeStamp.inSeconds;
  }
  bool operator < (CommentData m) {
    return timeStamp.inSeconds < m.timeStamp.inSeconds;
  }
  bool operator >= (CommentData m) {
    return timeStamp.inSeconds >= m.timeStamp.inSeconds;
  }
  bool operator <= (CommentData m) {
    return timeStamp.inSeconds <= m.timeStamp.inSeconds;
  }

}

class CommentDB {
  Future<Database> database;
  CommentDB() {
    getDatabasesPath().then((value) {
      database = openDatabase(
        join(value, "comment_database.db"),
        onCreate: (db, version) {
          return db.execute(
            "CREATE TABLE comments(id INTEGER PRIMARY KEY, video_id TEXT, title TEXT, content TEXT, timeStamp TEXT)"
          );
        },
        version: 1,
      );
    });
  }

  Future<void> insertComment(CommentData comment) async {
    final Database db = await this.database;

    await db.insert(
      'comments',
      comment.toMap(),
      conflictAlgorithm: ConflictAlgorithm.replace,
    );
  }

  Future<void> deleteDB() async {
    var databasesPath = await getDatabasesPath();
    String path = join(databasesPath, 'comment_database.db');

    await deleteDatabase(path);
  }

  Future<void> clearDB() async {
    final Database db = await database;
    db.rawDelete("DELETE FROM comments");
  }

  Future<List<CommentData>> comments() async {
    final Database db = await database;
    final List<Map<String, dynamic>> maps = await db.query('comments');

    return List.generate(maps.length, (i) {
      return CommentData(
        id: maps[i]['id'],
        videoId: maps[i]['video_id'],
        title: maps[i]['title'],
        content: maps[i]['content'],
        timeStamp: StringToDuration(maps[i]['timeStamp']),
      );
    });
  }
  Future<List<CommentData>> GetCommentsWithVideoId(String videoId) async {
    final Database db = await database;
    final List<Map<String, dynamic>> maps = await db.query('comments', orderBy: 'timeStamp ASC');

    return List.generate(maps.length, (i) {
      if(maps[i]['video_id'] == videoId) {
        return CommentData(
          id: maps[i]['id'],
          videoId: maps[i]['video_id'],
          title: maps[i]['title'],
          content: maps[i]['content'],
          timeStamp: StringToDuration(maps[i]['timeStamp']),
        );
      }
      else {
        return null;
      }
    });
  }

  Future<void> updateComment(CommentData comment) async {
    final db = await database;
    await db.update(
      'comments',
      comment.toMap(),
      where: "id = ?, video_id = ?",
      whereArgs: [comment.id, comment.videoId],
    );
  }

  Future<void> deleteComment(int id, String videoId) async {
    final db = await database;
    await db.delete(
      'comments',
      where: "id = ?, video_id = ?",
      whereArgs: [id, videoId],
    );
  }
}
