import 'package:flutter/material.dart';
import 'package:vimark/home.dart';
//import 'package:firebase_auth/firebase_auth.dart';
import 'package:vimark/login.dart';
import 'package:vimark/storage.dart';
import 'custom.dart';
import 'comment.dart';
import 'storage.dart';

void main() => runApp(App());

class App extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: FirstPage()
    );
  }
}
class FirstPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return SkipStatusBar(
      child: Scaffold(
        primary: false,
        body: Center(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Padding(
                padding: const EdgeInsets.symmetric(vertical: 20),
                child: Text(
                  "로그인 미구현",
                  style: TextStyle(
                      fontSize: 40,
                      fontWeight: FontWeight.bold
                  ),
                ),
              ),
              RaisedButton(
                child: Text("홈으로"),
                onPressed: () {
                  /*MemoDB memodb = MemoDB();
                  Future f() async {
                    await Future.delayed(Duration(seconds: 5), () {
                      memodb.clearDB();
                    });
                  }
                  f();*/

                  Navigator.pushReplacement(context, MaterialPageRoute(builder: (context) => HomePage()));
                  //Navigator.pushReplacement(context, MaterialPageRoute(builder: (context) => MemoPage(videoId: "1eb-AK3WS6Y")));
                },
              ),
            ],
          ),
        ),
      ),
    );
  }
}

/*class App extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: StreamBuilder<FirebaseUser>(
        stream: FirebaseAuth.instance.onAuthStateChanged,
        builder: (BuildContext context, AsyncSnapshot snapshot) {
          if(snapshot.hasData) {
            return MainPage(user: snapshot.data);
          }
          else {
            return LoginPage();
          }
        },
      ),
    );
  }
}*/
//MemoPage(videoId: "1eb-AK3WS6Y")

/*class MainPage extends StatefulWidget {
  FirebaseUser user;

  MainPage({this.user});

  @override
  _MainPageState createState() => _MainPageState();
}

class _MainPageState extends State<MainPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: RaisedButton(
          child: Container(
            height: 100,
            width: 100,
          ),
          onPressed: () {
            Navigator.pushReplacement(context, MaterialPageRoute(builder: (context) => MemoPage()));
          },
        ),
      ),
    );
  }
}*/