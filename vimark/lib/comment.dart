import 'package:flutter/material.dart';
import 'package:youtube_player_flutter/youtube_player_flutter.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:xml/xml.dart' as xml;
import 'custom.dart';
import 'storage.dart';
import 'dart:io';
import 'package:flutter/services.dart';


class CommentPage extends StatefulWidget {
  String videoId;
  CommentPage({Key key, this.videoId}) : super(key: key);

  @override
  _CommentPageState createState() => _CommentPageState();
}

class _CommentPageState extends State<CommentPage> {
  List<VideoMarkTexts> commentList = new List<VideoMarkTexts>();
  List<Widget> playerStackWidgetList;

  YoutubePlayerController _videoController;
  TextEditingController _commentTitleController;
  TextEditingController _commentContentController;
  Icon stateIcon = Icon(Icons.pause);
  Duration currentDuration;
  Duration deltaDuration;
  CommentData commentData;
  CommentDB commentDB;

  String timeProgress;
  bool isPlaying = true;
  bool isCommenting = false;
  double widgetOpacity = 1;
  double screenWidth;
  double screenHeight;
  int transformMilliseconds = 300;

  void initState() {
    super.initState();
    commentDB = CommentDB();
    _videoController = YoutubePlayerController();
    _commentTitleController = TextEditingController();
    _commentContentController = TextEditingController();
    currentDuration = Duration();
    deltaDuration = Duration();
    playerStackWidgetList = <Widget> [
      YoutubePlayer(
        context: context,
        videoId: widget.videoId,
        flags: YoutubePlayerFlags(
          autoPlay: true,
          showVideoProgressIndicator: false,
          hideFullScreenButton: true,
        ),
        aspectRatio: 16 / 9,
        onPlayerInitialized: (controller) {
          _videoController = controller;
          _videoController.play();
          //_controller.addListener(listener); listener is function : refer https://github.com/sarbagyastha/youtube_player_flutter/blob/master/example/lib/main.dart
        },
      ),
    ];
  }
  @override
  Widget build(BuildContext context) {
    screenWidth = MediaQuery.of(context).size.width;
    screenHeight = MediaQuery.of(context).size.height;
    return SkipStatusBar(
      child: Scaffold(
        body: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Stack(
                children: playerStackWidgetList,
              ),
            ] + (!isCommenting ? <Widget> [
              Expanded(
                child: AnimatedOpacity(
                  opacity: widgetOpacity,
                  duration: Duration(milliseconds: transformMilliseconds ~/ 2),
                  child: FutureBuilder(
                    future: commentDB.GetCommentsWithVideoId(widget.videoId),
                    builder: (context, snapshot) {
                      commentList.clear();
                      if(snapshot.hasData) {
                        List<CommentData> datas = snapshot.data;
                        datas.removeWhere((CommentData m) => m == null ? true : false);
                        print("DATAS:$datas");
                        for(CommentData m in datas) {
                          commentList.add(
                            VideoMarkTexts(
                              title: m.title,
                              content: m.content,
                              timeStamp: m.timeStamp,
                              controller: _videoController,
                            )
                          );
                        }
                      }
                      else {
                        return Center(child: CircularProgressIndicator());
                      }
                      return ListView(
                        padding: EdgeInsets.all(0),
                        scrollDirection: Axis.vertical,
                        children: commentList,
                      );
                    }
                  ),
                ),
              ),
              AnimatedOpacity(
                opacity: widgetOpacity,
                duration: Duration(milliseconds: transformMilliseconds ~/ 2),
                child: Container(
                  height: 80,
                  decoration: BoxDecoration(
                    boxShadow: [
                      BoxShadow(
                          color: Colors.grey,
                          blurRadius: 15,
                          spreadRadius: 3,
                          offset: Offset(0, -10)
                      )
                    ],
                  ),
                  child: Container(
                      decoration: BoxDecoration(
                        color: Colors.white,
                        borderRadius: BorderRadius.all(Radius.circular(10)),
                      ),
                      height: 70,
                      child: Column(
                        children: <Widget> [ //기본 상태
                          Expanded(
                            flex: 1,
                            child: Padding(
                              padding: EdgeInsets.symmetric(vertical: 4, horizontal: 5),
                              child: Row(
                                children: <Widget>[
                                  Expanded(
                                    child: RaisedButton(
                                      color: Colors.grey,
                                      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(5)),
                                      child: Text("여기를 눌러 메모 추가"),
                                      onPressed: () {
                                        currentDuration = Duration(seconds: _videoController.value.position.inSeconds);
                                        setState(() {
                                          timeProgress = DurationToString(currentDuration);
                                          widgetOpacity = 0;
                                          Future f() async {
                                            await Future.delayed(Duration(milliseconds: transformMilliseconds ~/ 2), () {
                                              setState(() {
                                                isCommenting = true;
                                                widgetOpacity = 1;
                                              });
                                            });
                                          }
                                          f();
                                          _videoController.pause();
                                          playerStackWidgetList.add(
                                            Container(
                                              color: Colors.black.withOpacity(0.5),
                                              child: Center(
                                                child: Container(
                                                  height: screenWidth * 9 / 16,
                                                  child: Text(timeProgress,
                                                    style: TextStyle(
                                                      fontSize: 40,
                                                      color: Colors.white,
                                                      shadows: [
                                                        Shadow(
                                                          blurRadius: 10,
                                                          color: Colors.black,
                                                        )
                                                      ],
                                                    ),
                                                  ),
                                                ),
                                              ),
                                            ),
                                          );
                                        });
                                      },
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          ),
                          Padding(
                            padding: EdgeInsets.symmetric(horizontal: 8),
                            child: Divider(
                              height: 2,
                              color: Colors.black,
                            ),
                          ),
                          Expanded(
                            flex: 1,
                            child: SizedBox(
                              width: 300,
                              child: Row(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: <Widget> [
                                  InkWell(
                                    child: Icon(Icons.fast_rewind),
                                    onTap: () {
                                      _videoController.seekTo(_videoController.value.position - Duration(seconds: 5));
                                    },
                                  ),
                                  InkWell(
                                    child: stateIcon,
                                    onTap: () {
                                      if(isPlaying) {
                                        isPlaying = false;
                                        _videoController.pause();
                                        setState(() {
                                          stateIcon = Icon(Icons.play_arrow);
                                        });
                                      }
                                      else {
                                        isPlaying = true;
                                        _videoController.play();
                                        setState(() {
                                          stateIcon = Icon(Icons.pause);
                                        });
                                      }
                                    },
                                  ),
                                  InkWell(
                                    child: Icon(Icons.fast_forward),
                                    onTap: () {
                                      _videoController.seekTo(_videoController.value.position + Duration(seconds: 5));
                                    },
                                  ),
                                ],
                              ),
                            ),
                          )
                        ],
                      )
                  ),
                ),
              ) // Container
            ] : <Widget> [
              Expanded(
                child: AnimatedOpacity(
                  opacity: widgetOpacity,
                  duration: Duration(milliseconds: transformMilliseconds ~/ 2),
                  child: Padding(
                    padding: const EdgeInsets.all(10.0),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        TextFormField(
                          textInputAction: TextInputAction.next,
                          keyboardType: TextInputType.multiline,
                          maxLines: null,
                          controller: _commentTitleController,
                          decoration: InputDecoration(
                            border: InputBorder.none,
                            hintText: "메모 제목을 입력합니다.",
                            hintStyle: TextStyle(
                              fontSize: 36,
                              fontWeight: FontWeight.bold,
                            ),
                          ),
                          style: TextStyle(
                            fontSize: 36,
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                        Text(currentDuration.toString()),
                        TextFormField(
                          textInputAction: TextInputAction.newline,
                          keyboardType: TextInputType.multiline,
                          maxLines: null,
                          controller: _commentContentController,
                          decoration: InputDecoration(
                            border: InputBorder.none,
                            hintText: "메모를 입력하세요. 첫 줄은 요약 및 제목",
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
              ),
              AnimatedOpacity(
                opacity: widgetOpacity,
                duration: Duration(milliseconds: transformMilliseconds ~/ 2),
                child: Divider(
                  height: 3,
                  thickness: 1,
                ),
              ), // Divider
              AnimatedOpacity( // Buttons
                opacity: widgetOpacity,
                duration: Duration(milliseconds: transformMilliseconds ~/ 2),
                child: Container(
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.end,
                    children: <Widget>[
                      Padding(
                        padding: const EdgeInsets.symmetric(vertical: 8, horizontal: 4),
                        child: InkWell(
                            onTap: () {
                              if(_commentTitleController.value.text.isEmpty && _commentContentController.value.text.isEmpty) {
                                Fluttertoast.showToast(
                                    msg: "메모를 입력해주세요.",
                                    toastLength: Toast.LENGTH_SHORT,
                                    gravity: ToastGravity.CENTER,
                                    timeInSecForIos: 1,
                                    backgroundColor: Colors.black,
                                    textColor: Colors.white,
                                    fontSize: 16.0
                                );
                                return;
                              }
                              String title = _commentTitleController.value.text;
                              String content = _commentContentController.value.text;
                              title = (title != "" && title != null) ? title : content.split('\n')[0];
                              Duration pos = _videoController.value.position;
                              commentData = CommentData(
                                id: DateTime.now().difference(DateTime(2000, 06, 29)).inSeconds,
                                videoId: widget.videoId,
                                title: title,
                                content: content,
                                timeStamp: currentDuration
                              );
                              setState(() {
                                commentDB.insertComment(commentData);
                                /*commentList.add(VideoMarkTexts(
                                  controller: _videoController,
                                  timeStamp: commentData.timeStamp,
                                  title: commentData.title,
                                  content: commentData.content,
                                ));*/
                                widgetOpacity = 0;
                                playerStackWidgetList.removeRange(1, 2);
                              });
                              Future f() async {
                                await Future.delayed(Duration(milliseconds: transformMilliseconds ~/ 2), () {
                                  setState(() {
                                    isCommenting = false;
                                    widgetOpacity = 1;
                                  });
                                });
                              }
                              f();
                              _commentTitleController.clear();
                              _commentContentController.clear();
                              _videoController.play();
                            },
                            child: Container(
                              height: 30,
                              width: 70,
                              decoration: BoxDecoration(
                                  shape: BoxShape.rectangle,
                                  border: Border.all(
                                    color: Colors.black,
                                  ),
                                  borderRadius: BorderRadius.all(Radius.circular(15))
                              ),
                              child: Center(
                                  child: Text("저장하기")
                              ),
                            )
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.symmetric(vertical: 8, horizontal: 4),
                        child: InkWell(
                          onTap: () {
                            setState(() {
                              widgetOpacity = 0;
                              playerStackWidgetList.removeRange(1, 2);
                            });
                            Future f() async {
                              await Future.delayed(Duration(milliseconds: transformMilliseconds ~/ 2), () {
                                setState(() {
                                  isCommenting = false;
                                  widgetOpacity = 1;
                                });
                              });
                            }
                            f();
                            _commentContentController.clear();
                            _commentTitleController.clear();
                            _videoController.play();
                          },
                          child: Container(
                            height: 30,
                            width: 70,
                            decoration: BoxDecoration(
                                shape: BoxShape.rectangle,
                                border: Border.all(
                                  color: Colors.black,
                                ),
                                borderRadius: BorderRadius.all(Radius.circular(15))
                            ),
                            child: Center(
                                child: Text("취소하기")
                            ),
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
              ) // Buttons
            ])
        ),
      ),
    );
  }
}


class VideoMarkTexts extends StatelessWidget {
  final Duration timeStamp;
  final String title;
  final String content;
  final YoutubePlayerController controller;
  VideoMarkTexts({this.controller, this.timeStamp, this.title, this.content});

  @override
  Widget build(BuildContext context) {
    return InkWell(
      child: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 8, vertical: 4),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            GestureDetector(
              onTap: () {
                controller.seekTo(timeStamp);
              },
              child: Text(
                  DurationToString(timeStamp),
                  style: TextStyle(color: Colors.blue)
              ),
            ),
            InkWell(
              onTap: () {
                //do something
                //when tap whole these widget
              },
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  rearrangedTitle(this.title),
                  Row(
                    children: <Widget>[
                      Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: Container(
                          width: 1.5,
                          height: 36,
                          color: Colors.black,
                        ),
                      ),
                      Expanded(
                        flex: 10,
                        child: Text(
                          this.content,
                          style: TextStyle(
                              color: Colors.grey,
                              fontSize: 14
                          ),
                          softWrap: true,
                          maxLines: 2,
                        ),
                      ),
                    ],
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}

Text rearrangedTitle(String text) {
  if(text.length > 30)
    text = text.substring(0, 30) + "...";
  return Text(
    text ?? "NULL TEXT",
    maxLines: 1,
    style: TextStyle(fontSize: 18),
  );
}
